Schoolkit, une trousse pour la géométrie
========================================

Dans une trousse scolaire, on trouve des outils de géométrie.

Ce paquet fournit une petite bibliothèque en Javascript, qui implémente
ces outils pour l'écran, sous forme de graphismes vectoriels SVG
interactifs. Cette biliothèque est écrite en JavaScript « pur », et ne 
nécessite pas de dépendances.

Les outils actuellement implémentés sont :

 - le double décimètre
 - le rapporteur
 - l'équerre

On peu facilement ajouter d'autres outils en produisant des fichier SVG,
par exemple à l'aide d'Inkscape. Le mode d'emploi et la démonstration
apparaissent à l'URL http://localhost/javascript/schoolkit/index.html, 
sous réserve qu'un serveur web comme Apache2 soit installé aussi.

Utilisation de Schoolkit
------------------------

Téléchargez une archive de ce dépôt, ou clonez-le à l'aide de `git`.

Le fichier `index.htm` permet de faire une démonstration de la bibliothèque.

Le fichier `index.html` est prévu pour une démonstration identique, à
ceci près que les fichier `schoolkit.js` est censé être installé dans
le répertoire `/usr/share/javascript/schoolkit`, ce qui est le cas si
on utilise le paquet Debian `libjs-schoolkit`.

Examen du code qui met en place un outil
----------------------------------------

``` JavaScript
      let p = new GeomPaper("canvas_container1", 1000, 600);
```

Quand on instancie la classe `GeomPaper` on utilise trois paramètres :

 - l'identifiant d'un élément `<div>` où insérer la zone graphique
 - la largeur de la zone graphique en pixels
 - la hauteur de la zone graphique en pixels



``` JavaScript
      let o = new DoubleDecimetre(p, 105, 100, "dd01")
      p.addTool(o2, new DOMPoint(100,100), 3);
```

Les deux lignes de code ci-dessus mettent en place un double décimètre,
centré sur le point (100,100), à l'échelle 3. Le double décimètre est dans
ce cas généré par un court algorithme de la bibliothèque, directement.

 - on instancie la classe `DoubleDecimetre` ;
 - on ajoute l'instance de `DoubleDecimetre` à l'instance de `GeomPaper`,
   en précisant une position (facultative) pour le centre de rotation,
   et une échelle (facultative).

Code pour utiliser un fichier SVG indépendant
---------------------------------------------

Voici un exemple de code qui met en place un rapporteur fourni par le fichier 
`rapporteur01.svg`, que fournit la paquet debian `libjs-schoolkit` à 
l'emplacement `/usr/share/javascript/schoolkit/rapporteur01.svg` :

``` JavaScript
                let o1 = new GeomToolFromFile(p, "rapporteur01", "/javascript/schoolkit/rapporteur01.svg",
				             new DOMPoint(500,300), 1);
```

Ceci place le rapporteur centré sur le point (500,300), 
avec un grandissement de 1.

Ajouter un nouvel outil géométrique
-----------------------------------

N'importe quel fichier SVG est utilisable, à **quatre conditions** :

 1. il doit contenir un groupe (élément `<g>`) avec l'attribut `id="layer1"` ;
    ce groupe contient les trois éléments suivants, au moins :
 2. un cercle (élément `<circle>`) dont les attributs `cx` et `cy` définissent
    le centre de rotation de l'outil ; il doit avoir l'attribut `id="centre"` ;
 3. un élément graphique quelconque, avec l'attribut `id="rotation_handle"` ;
    comme il sert à contrôler la rotation de l'outil par tirer-glisser, il faut
    éviter qu'il n'empiète sur le centre de rotation ;
 4. un élément graphique quelconque, avec l'attribut `id="translation_handle"` ;
    il sert à contrôler la translation de l'outil par tirer-glisser.

Le groupe d'identifiant `id="layer1"` peut contenir ensuite n'importe quel
nombre d'éléments graphiques, ceux-ci se déplaceront comme le reste de l'outil
géométrique selon le contrôle de la souris.
