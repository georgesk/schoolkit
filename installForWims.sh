#! /bin/sh

schoolkitOK=$(dpkg -l libjs-schoolkit| grep ii)

if  [ -n "$schoolkitOK" ]; then
    echo "La dépendance libjs-schoolkit est présente."
else
    echo "Il faut installer la dépendance libjs-schoolkit."
    exit 1
fi

for l in schoolkit; do
    rm -rf /var/lib/wims/public_html/scripts/js/$l
    ln -s /usr/share/javascript/$l /var/lib/wims/public_html/scripts/js/$l
done

if [ -f /var/lib/wims/public_html/scripts/js/schoolkit/schoolkit.js ]; then
    echo "Installation réussie."
else
    echo "L'installation a raté, voir dans /var/lib/wims/public_html/scripts/js"
fi
