Schoolkit, geometrical tools for the school
===========================================

In a school kit, one can find geometrical tools.

This package provides a small JavaScript library, implementing those
tools for the screen, as interactive vector SVG graphic groups.
This library is written in plain JavaScript, so it does not rely on
dependencies.

Currently implemented tools are:

 - the ruler (20 cm long)
 - the protractor
 - the square

One can easily add other tools by writing SVG files, for example with
Inkscape. A short user manual and a demonstration are available at
http://localhost/javascript/schoolkit/index.html, 
provided a web server like apache2 is also installed.

Using Schoolkit
---------------

Download an archive from this repository, or clone it with `git`.

The file `index.htm` allows one to demonstrate the library, in development
mode.

The files `index*.html` are meant for a similar demonstration, but the file
`schoolkit.js` must be installed in the directory
`/usr/share/javascript/schoolkit`, which happens automatically when one
installs the Debian package `libjs-schoolkit`.

Watching the code to provide a tool
-----------------------------------

``` JavaScript
      let p = new GeomPaper("canvas_container1", 1000, 600);
```

When one makes an instance of the class `GeomPaper`, three parameters are
necessary:

 - the identifier of a `<div>` element where the graphic area will be inserted
 - the width of the graphic area in pixel unit
 - the height of the graphic area in pixel unit



``` JavaScript
      let o = new DoubleDecimetre(p, 105, 100, "dd01")
      p.addTool(o2, new DOMPoint(100,100), 3);
```

The two lines of code above create the ruler (20 cm long), centered on
the point (100,100), with scale factor 3. The double decimeter is then
generated by a short algorithm provided by the library.

 - one make an instance from the class `DoubleDecimetre`;
 - this instance of `DoubleDecimetre` is added to the instance of `GeomPaper`,
   with the precision of a position (optional) for its rotation center,
   and of a scale factor (optional).

Code to use an independent SVG file
-----------------------------------

Here is an example of code to insert a protractor provided by the file
`rapporteur01.svg`, which comes with the debian package `libjs-schoolkit` at
the location `/usr/share/javascript/schoolkit/rapporteur01.svg`:

``` JavaScript
                let o1 = new GeomToolFromFile(p, "rapporteur01", "/javascript/schoolkit/rapporteur01.svg",
				             new DOMPoint(500,300), 1);
```

This creates a protractor centered on point (500,300), 
with a zoom factor unity (the default value).

Create a new geometrical tool
-----------------------------

Any SVG file can be used, provided **four conditions** are met:

 1. it must contain a group (element `<g>`) with the attribute `id="layer1"`;
    this group owns the three following elements, at least:
 2. a circle (element `<circle>`) whose attributes `cx` and `cy` define
	the rotation center of the tool; it must have the attribute `id="centre"`;
 3. whatever graphic element, with the attribute `id="rotation_handle"`;
	as it will control the rotation of the tool by means of mouse-dragging,
	it should not overlap the center of rotation;
 4. whatever graphic element, with the attribute `id="translation_handle"`;
	it will control the translation of the tool by means of mouse-dragging.

The group with `id="layer1"` can contain later any number of graphic elements,
those will move as the other parts of the geometrical tool under mouse
control.
